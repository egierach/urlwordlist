# REQUIREMENTS #

Please write an application with 3 pages.  There should be an index page, a form page, and a result/form response page.

The form page should request a URL as the only field, with some amount of validation.

The result/form response page should:
Check to see if this URL has previously been scanned,  If so, display the stored results from the previous scan (see below).
Otherwise, read the URL and provide a list of the 10 most common words that appear as text at that URL with the number of times they appear and store the result.
The index page should list the previously scanned URLs, the most common word at the URL and the number of times it appeared, and a way to get to the result page for that particular URL.