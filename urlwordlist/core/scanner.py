"""
Does the work of downloading and parsing web pages at URLs.

Uses a basic regex to pick out groups of "word" characters,
which are the set of alphanumerics plus underscore.  I chose this
character class over a traditional [A-Za-z] class as it allows 
accented characters and characters from other alphabets.
"""
import requests
from requests.exceptions import HTTPError, ConnectionError
from HTMLParser import HTMLParser
from collections import Counter
import logging
import re

logger = logging.getLogger(__name__)

class Scanner(HTMLParser):
    word_regex = re.compile(r'\w+', re.MULTILINE|re.UNICODE|re.LOCALE)
    words = Counter() # a dictionary subclass that specializes in counting occurrences of keys
    recording = True

    def __init__(self, *args, **kwargs):
        self.words = Counter()
        self.recording = True
        # HTMLParser is an old-style class, so call its constructor explicitly
        HTMLParser.__init__(self, *args, **kwargs)

    def handle_starttag(self, tag, attrs):
        """
        Shuts off word processing for <script> and <style> blocks.
        """
        if tag.lower() in ['script', 'style']:
            self.recording = False

    def handle_endtag(self, tag):
        """
        Re-enables word processing after </script> and </style> closing tags.
        """
        if tag.lower() in ['script', 'style']:
            self.recording = True

    def handle_data(self, data):
        """
        Finds all word-looking patterns in a chunk of text and adds them to the tally.
        """
        if self.recording:
            self.words.update(self.word_regex.findall(data.lower()))

class ScanError(Exception):
    """
    Errors caught in this module are either handled or summarized, paraphrased and re-raised
    as a ScanError, just to cut down on the error handling elsewhere.
    """
    pass

def scan_url(url, words_to_return=10):
    """
    Requests the page at the URL, feeds its content to the parser
    and returns the top "words_to_return" words, along with their
    respective occurrences.

    Raises ScanError if anything bad happens.
    """
    try:
        response = requests.get(url)
        response.raise_for_status()
    except ConnectionError as c:
        logger.exception(c)
        raise ScanError("Unable to connect to host for URL {}".format(url))
    except HTTPError as h:
        logger.exception(h)
        raise ScanError("Received HTTP {} Error at URL {}".format(response.status, url))
    if not response.text:
        raise ScanError("No content found at URL {}".format(url))
    parser = Scanner()
    parser.feed(response.text)
    if not len(parser.words):
        raise ScanError("No words found at URL {}".format(url))
    return parser.words.most_common(words_to_return)

