"""
Processes incoming HTTP requests, as directed by urls.py.
"""
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.template import loader, RequestContext, TemplateDoesNotExist
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
import logging
from core.models import ScanResult
from core.forms import ScanForm
from core.scanner import scan_url, ScanError

logger = logging.getLogger(__name__)

def display_home(request):
    """
    displays a list of previously-scanned URLS, along with the most-repeated word for each.
    """
    context_vars = {}
    context_vars['results'] = ScanResult.objects.all().order_by('-created_dt')
    return render(request, 'home.html', context_vars)

def display_scan_form(request):
    """
    displays and handles input from a form allowing the user to enter a URL to scan.
    """
    if request.method == 'POST':
        form = ScanForm(request.POST)
        scan_result = None
        if form.is_valid():
            try:
                scan_result = ScanResult.objects.get(url__iexact=form.cleaned_data['url'])
            except ObjectDoesNotExist:
                try:
                    top_words = scan_url(form.cleaned_data['url'], settings.NUM_WORDS_PER_URL)
                except ScanError as e:
                    logger.exception(e)
                    form.add_error(None, e.message)
                else:
                    scan_result = ScanResult(url=form.cleaned_data['url'])
                    scan_result.save()
                    for word, num_occurrences in top_words:
                        scan_result.words.create(word=word, num_occurrences=num_occurrences)
        else:
            logger.debug('Scan form submission errors: {}'.format(form.errors))
        if scan_result:
            return HttpResponseRedirect(reverse('core:result', kwargs={'result_id': scan_result.id}))
    else:
        form = ScanForm()
    return render(request, 'scan.html', {'form': form})

def display_scan_result(request, result_id):
    """
    displays the top ten words for a scanned URL, along with the number of appearances for each.
    """
    context_vars = {}
    try:
        context_vars['result'] = ScanResult.objects.get(id=result_id)
    except ObjectDoesNotExist as e:
        logger.exception(e)
        raise Http404
    return render(request, 'result.html', context_vars)

def display_test(request, test_id):
    """
    displays test web pages.
    """
    try:
        template = loader.get_template('test_{}.html'.format(test_id))
    except TemplateDoesNotExist as e:
        logger.exception(e)
        raise Http404
    return HttpResponse(template.render(RequestContext(request)))
