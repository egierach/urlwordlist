from django.conf.urls import patterns, include, url

urlpatterns = patterns('core.views',
    url(r'^/?$', 'display_home', name='home'),
    url(r'^scan/?$', 'display_scan_form', name='scan'),
    url(r'^results/(?P<result_id>[0-9]+)/?$', 'display_scan_result', name='result'),
    url(r'^tests/(?P<test_id>[0-9]+)/?$', 'display_test', name='test'),
)
