"""
Data models for the application.
"""
from django.db import models

class ScanResult(models.Model):
    """
    Entity representing a single URL submission and scan event.
    """
    url = models.URLField(max_length=1000)
    created_dt = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = 'core'

    def top_word(self):
        """
        Returns the single most prevalent word for this scan result.
        """
        return self.words.order_by('-num_occurrences').first()
    
class ScanResultWord(models.Model):
    """
    Entity representing one of the N top words for a scan event.
    """
    result = models.ForeignKey('ScanResult', related_name='words', related_query_name='word')
    word = models.CharField(max_length=50)
    num_occurrences = models.IntegerField()

    class Meta:
        app_label = 'core'
