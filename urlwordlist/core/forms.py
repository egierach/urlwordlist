"""
Represents an HTML form and provides some under-the-covers validation
by virtue of picking appropriate field types.
"""
from django import forms

class ScanForm(forms.Form):
    url = forms.URLField(label='URL of Web Page to Scan', max_length=1000)

